1.  创建名为custom_pwm_ip的工程，创建IP核并进行配置，创建包装一个新的IP核，基于AXI第四代协议创建IP核，命名为ax_pwm，总线接口类型选择Lite。
2.  修改IP核，添加ax_pwm.v文件，将模块代码嵌入并保存，打开IP-XACT，更新文件，IP核已经被修改，重新包装IP核，关闭掉临时工程。
3.  添加我们刚刚创建的IP核，自动连接，重新布局，将pwm输出引出来，重新布局，验证设计规则，保存，创建HDL文件，Generate Output Products，创建约束。
4.  生成比特流文件，导出硬件（包含比特流文件），打开SDK，创建名为pwm_test的工程，模板选择Hello World，对helloworld.c文件进行修改，运行程序，看到LED灯由暗到亮的效果。
